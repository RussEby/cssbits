# README #

This project is a set of static web pages.

### What is this repository for? ###

* A space to store demos of CSS tricks and examples
* [CSS Demos](https://cssdemos.ebyworld.com)

### How do I get set up? ###

Since this is a set of static pages, just place in a folder and use your favorite pages.

### Contribution guidelines ###

The main index is just list of each demo. The index will contain the name a small description and a link to the blog or video that inspired it.

### Who do I talk to? ###

RussEby would love to add whatever CSS Demos and Example you might come up with.