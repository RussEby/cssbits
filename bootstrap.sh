#!/usr/bin/env bash

apt-get update

em -rf /var/www
ln -fs /vagrant /var/www

service apache2 restart